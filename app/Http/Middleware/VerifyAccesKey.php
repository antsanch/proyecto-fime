<?php

namespace App\Http\Middleware;

use Closure;

class VerifyAccesKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //obtenemos el api-key q el usuario envía
        $key = $request->headers->get('api_key');
        //si coinciden con el valor almacenado en la aplicacion esta se sigue ejecutando
        if (isset($key) == env('API_KEY')) {
            return $next($request);
        } else {
          //si falla devolvemos el mensaje de error
          return response()->json(['error' => 'unauthorized'], 401);
        }


    }
}
