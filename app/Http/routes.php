<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('users', 'UserController');

//Route::resource('pacientes', 'PacienteController');

Route::post('oauth/request', function() {
    return Response::json(Authorizer::issueAccessToken());
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['oauth']], function () {
    Route::resource('users', 'UserController');
    Route::resource('pacientes', 'PacienteController');
});

Route::group(['middleware' => ['web']], function () {
    //
});
