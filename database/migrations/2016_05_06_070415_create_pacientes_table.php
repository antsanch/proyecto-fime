<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->char('CURP', 18)->unique();
            $table->string('primer_apellido', 50);
            $table->string('segundo_apellido', 50);
            $table->string('nombre', 50);
            $table->char('fecnac', 8);
            $table->char('edonac', 2);
            $table->char('sexo', 1);
            $table->char('nacorigen', 3);
            $table->char('folio', 18);
            $table->char('edo', 2);
            $table->char('mun', 3);
            $table->char('loc', 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pacientes');
    }
}
