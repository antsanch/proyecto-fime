<?php
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 6/05/16
 * Time: 03:37 AM
 */
class AdminTableSeeder extends Seeder {

    public function run() {

            \DB::table('users')->insert(array(
                'first_name' => 'Pedro',
                'last_name' => 'Pool',
                'email' => 'pedropool13@gmail.com',
                'password' => \Hash::make('secret'),
                'categoria' => '1',
                'permisos' => '1'

            ));
        }
}