@extends('layout')

@section('content')
    <h2>Editar usuario {{ $user -> first_name }}</h2>
    {!! Form::model($user, ['route' => ['users.update', $user],'method' => 'PUT']) !!}
        @include('users.partials.fields')
        <button type="submit" class="btn btn-default">Actualizar</button>
    {!! Form::close() !!}
    @include('users.partials.delete')

  @endsection
