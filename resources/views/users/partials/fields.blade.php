<div class="form-group">
    {!! Form::label('email', 'Correo electŕonico') !!}
    {!!  Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su email']) !!}
</div>
<div class="form-group">
    {!! Form::label('password', 'Constraseña') !!}
    {!!  Form::password('password', ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su contraseña']) !!}
</div>
<div class="form-group">
    {!! Form::label('first_name', 'Nombre') !!}
    {!!  Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su nombre']) !!}
</div>
<div class="form-group">
    {!! Form::label('last_name', 'Apellido') !!}
    {!!  Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su apellido']) !!}
</div>
<div class="form-group">
    {!! Form::label('categoria', 'Categoría') !!}
    {!!  Form::select('categoria', ['' => 'Seleccione tipo', '1' => '1', '2' => '2', '3' => '3'], null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('permisos', 'Permisos') !!}
    {!!  Form::select('permisos', ['' => 'Seleccione tipo', '1' => '1', '2' => '2', '3' => '3'], null, ['class' => 'form-control']) !!}
</div>
