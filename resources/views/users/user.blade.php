@extends('layout')

@section('content')
    <h1>Usuarios en el sistema</h1>
    <p>Hay {{$users ->total()}} usarios.</p>
    <p>
        <a class="btn btn-info" href="{{ route('users.create') }}" role="button">
            Crear Usuario
        </a>
    </p>
    @include ('users.partials.table')
    {{$users -> render()}}

    {!! Form::open(['route' => ['users.destroy', 'USER_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
    {!! Form::close() !!}

@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-delete').click(function(e) {

                e.preventDefault();

                var row = $(this).parents('tr');
                var id = row.data('id');
                var form = $('#form-delete');
                var url = form.attr('action').replace('USER_ID', id);
                var data = form.serialize();

                row.fadeOut();

                $.post(url, data, function(result) {
                    alert(result);
                });
            });
        });

    </script>
@endsection