@extends('layout')

@section('content')

    {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
        @include('users.partials.fields')
        <button type="submit" class="btn btn-default">Crear</button>
    {!! Form::close() !!}


@endsection