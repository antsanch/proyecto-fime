@extends('layout')

@section('content')
         <h2>Editar paciente {{ $paciente -> nombre }}</h2>
    {!! Form::model($paciente, ['route' => ['pacientes.update', $paciente],'method' => 'PUT']) !!}
@include('pacientes.partials.fields')
        <button type="submit" class="btn btn-default">Actualizar</button>
    {!! Form::close() !!}
@include('pacientes.partials.delete')


@endsection